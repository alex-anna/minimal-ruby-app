![logo](/picdir/Logo.png)
---------------
**Agenda:**

- Introduction
- Overview of Alex & Anna Co. 
- Live Demo - Diving into GitLab (A Single Application for Whole Devops Lifecycle)
- Quick Overview of Current vs Simplified Technology Stack
- Wrap up: Action Items and Discussion for next steps

---------------

**Current vs Simplified Technology Stack:**

![diagram](/picdir/SA_demo.png)

---------------

![GitLab DevOps LifeCycle Graphic](https://about.gitlab.com/images/press/devops-lifecycle.svg)

Resources: 

- https://about.gitlab.com/devops-tools/jira-vs-gitlab/
- https://about.gitlab.com/devops-tools/github-vs-gitlab/
- https://about.gitlab.com/devops-tools/jenkins-vs-gitlab/
